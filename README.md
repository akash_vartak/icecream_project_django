# Icecream_Project_Django
This is a learning project for Django.

###Usage
**NOTE: Make sure you have  `pip` and `virtualenv` installed.**

**If not then install by *(for Ubuntu)*

`apt-get install pip`

`pip install virtuelenv`

**Both installations should be system-wide**


1. cd into the directory `IcecreamProject2` 

2. start vitual environment by `source bin/activate`    

3. cd into `Project`    

4. run development server by    
`python manage.py runserver` *(server starts at default: 127.0.0.1:8000)*     

     `python manage.py runserver`*`<port_number>`* *(server starts at default: 127.0.0.1:<port_number>)*   

     `python manage.py runserver`*`<IP>`*`:`*`<port_number>`* *(server starts given IP : port_number)*    


####Alternative
If step 2 dosn't work then follow these steps:    

1. create a folder `IcecreamProject2`    

2. cd out one level from the folder     

3. Run `virtualenv IcecreamProject2`
    This should install pip into the directory and setup the virtual enviromnment   

4. cd into the directory the activate virtual environment by `source bin/activate`    

5. Install Django by `pip install django==1.8` **Make sure the version installed is 1.8**     

6. Run `django-admin startproject Project`     

7. cd into `Project`    

8. Run `python manage.py startapp icecream`    

9. Copy all files (except migrations folder) from      

https://github.com/akash-vartak/Icecream_Project_Django/tree/master/IcecreamProject2/Project/icecream into     

`icecream` folder    

10. cd into `icecream` and run     

`python manage.py makemigrations`     

`python manage.py migrate`