from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'Project.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^icecream/', include('icecream.urls', namespace="icecream")),
    url(r'^admin/', include(admin.site.urls)),
)