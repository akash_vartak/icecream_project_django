from django.db import models

# Create your models here.

class IcecreamName(models.Model):
    cream_name=models.CharField(max_length = 100)
    cream_count=models.IntegerField(default=100)
    
    def __str__(self):
        return self.cream_name