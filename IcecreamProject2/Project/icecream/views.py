from django.shortcuts import render

from .models import IcecreamName
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.template import loader
from django.shortcuts import get_object_or_404, render
from django.core.urlresolvers import reverse
from django.views import generic
from django.utils import timezone
# Create your views here.

class IndexView(generic.ListView):
    template_name = 'icecream/index.html'
    context_object_name = 'latest_icecream_list'
    
    def get_queryset(self):
        return IcecreamName.objects.all().order_by('-cream_count')

class DetailView(generic.DetailView):
    model = IcecreamName
    template_name = 'icecream/detail.html'
    def get_queryset(self):
        return IcecreamName.objects.all()

class ResultsView(generic.DetailView):
    model = IcecreamName
    template_name = 'icecream/results.html'

def buy(request, ice_id):
    p = get_object_or_404(IcecreamName, pk=ice_id)
    #selected_choice = p.objects.get(pk=request.POST['cream_name'])
    if(p.cream_count==0):
        return render(request, 'icecream/detail.html', {
            'ice': p,
            'error_message': "No more scoops available",
        })
    else:
        p.cream_count -= 1
        p.save()
        return render(request, 'icecream/results.html', {
            'ice': p,
            'scoops_left': p.cream_count,
        })
        #return HttpResponseRedirect(reverse("icecream:results", args=(p.id,)))