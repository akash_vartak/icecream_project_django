from django.contrib import admin

from .models import IcecreamName
# Register your models here.

class IcecreamNameAdmin(admin.ModelAdmin):
    fieldsets=[
        ('Icecream Name', {'fields':['cream_name']}),
        ('Count Information', {'fields':['cream_count']}),
    ]
    list_display=('cream_name', 'cream_count')
    list_filter=['cream_count']
    search_fields=['cream_name']


admin.site.register(IcecreamName, IcecreamNameAdmin)